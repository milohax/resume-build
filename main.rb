require_relative "lib/config"
require "haml"
require 'fileutils'

ROOT_DIR = File.dirname(__FILE__)

puts "Generating configuration.."
config = Config.new()

begin
  yaml = config.read_resume_yaml()
rescue StandardError => e
  puts e.message
  return
end

puts "Checking config.."
if !config.stylesheet_file_exists?
  puts "Error: Stylesheet file doesn't exist: #{config.stylesheet_file}"
  exit 1
elsif !config.template_file_exists?
  puts "Error: Template file doesn't exist: #{config.template_file}"
  exit 1
elsif !config.yaml_source_dir_exists?
  puts "Error: YAML source dir doesn't exist: #{config.yaml_source}"
  exit 1
end

puts "Using YAML source directory: #{config.yaml_source}"
puts "Using template: #{config.template_file}"
puts "Using stylesheet: #{config.stylesheet_file}"
puts "Rendering HTML.."

engine = Haml::Engine.new(config.template)
html = engine.render(Object.new, { :@config => config, :@yaml => yaml})

output_directory = File.expand_path("#{ROOT_DIR}/public", File.dirname(__FILE__))
stylesheet = config.stylesheet_file
destination_stylesheet = "#{output_directory}/#{stylesheet}" 

puts "Copying output files to #{output_directory}"
FileUtils.mkdir_p("public/stylesheets") unless File.exists?("public/stylesheets")
File.write("#{output_directory}/index.html",html)
FileUtils.cp stylesheet, destination_stylesheet


